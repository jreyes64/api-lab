<?php
include "conectarpgsql.php";
$datos = $conexion->query("SELECT * FROM Uestudiantes");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>TABLA</title>
	<link rel="stylesheet" href="css.php" type="text/css">
</head>
<body>
	<div class="texto1">
    <p id="texto">LISTA ESTUDIANTES</p>
</div>
    <table style="width:65%", id="mov">
    	<tr>
    		<th>ID</th>
            <th>Nombre</th>
            <th>Codigo</th>
            <th>Celular</th>
            <th>Correo</th>
    	</tr>
    	<tbody>
    		<?php foreach($datos as $i=>$tablaE){?>
               <tr>                         
                  <td><?php echo $tablaE['num'] ?></td>
                  <td><?php echo $tablaE['nombre'] ?></td>
                  <td><?php echo $tablaE['codigo'] ?></td>
                  <td><?php echo $tablaE['celular'] ?></td>
                  <td><?php echo $tablaE['correo'] ?></td> 
               </tr>
            <?php } ?>
    	</tbody>
    </table>
</body>
</html>
